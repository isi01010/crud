@include('inc.header')

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<legend>CRUD Assignment</legend>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					@if(session('info'))
					<div class="alert alert-success">
						{{session('info')}}
					</div>
					@endif
				</div>
			</div>
							<table class="table table-hover">
					  <thead>
					    <tr>
					      <th scope="col">ID</th>
					      <th scope="col">TITLE</th>
					      <th scope="col">DESCRIPTION</th>
					      <th scope="col">ACTION</th>
					    </tr>
					  </thead>
					  <tbody>
					    @if(count($articles) > 0)
					    	@foreach($articles->all() as $article)
					    
					    <tr class="table-Secondary">
					      <td scope="row">{{$article->id}}</td>
					      <td>{{$article->title}}</td>
					      <td>{{$article->description}}</td>
					      <td>
					      	
	<a href='{{url("/read/{$article->id}")}}'' class="btn btn-primary">Read</a>
	<a href='{{url("/update/{$article->id}")}}''  class="btn btn-success">Update</a>
	<a href='{{url("/delete/{$article->id}")}}'' class="btn btn-danger">Delete</a>
					      </td>
					    </tr>
					    @endforeach
					@endif
				 </tbody>
</table>

	</div>
</div>
	
</div>
@include('inc.footer')