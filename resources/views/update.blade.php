@include('inc.header3')


<div class="container">
		<div class="row">
			<div class="col-md-6">
				
				<form method="POST" action="{{url('/edit',array($articles->id))}}">
					  <fieldset>
					 {{csrf_field()}}


					    <legend>CRUD Assignment</legend>
					 @if(count($errors)> 0)
					 		@foreach($errors->all() as $error)
					 		<div class="alert alert-danger">
					 			{{$error}}
					 		</div>
					 		@endforeach
					 @endif   
					    
					    <div class="form-group">
					      <label for="inputEmail1">Title</label>
<input type="text" name="title" class="form-control" id=inputEmail1" value="<?php echo $articles->title; ?>" aria-describedby="emailHelp" placeholder="Title">
					      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					    </div>
					    <div class="form-group">
					      <label for="inputPassword">Description</label>

	<textarea name="description" class="form-control" placeholder="Description"><?php echo $articles->description; ?></textarea>
					    </div>
					    
					    
					    <fieldset class="form-group">
				<button type="submit" class="btn btn-primary">Update</button>
					    
<a href="{{url('/')}}" class="btn btn-danger">Back</a>

					  </fieldset>
				</form>

			</div>
		</div>
	</div>

	
@include('inc.footer')

 